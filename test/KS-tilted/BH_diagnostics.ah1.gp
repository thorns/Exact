# apparent horizon 1/1
#
# column  1 = cctk_iteration
# column  2 = cctk_time
# column  3 = centroid_x
# column  4 = centroid_y
# column  5 = centroid_z
# column  6 = min radius
# column  7 = max radius
# column  8 = mean radius
# column  9 = quadrupole_xx
# column 10 = quadrupole_xy
# column 11 = quadrupole_xz
# column 12 = quadrupole_yy
# column 13 = quadrupole_yz
# column 14 = quadrupole_zz
# column 15 = min x
# column 16 = max x
# column 17 = min y
# column 18 = max y
# column 19 = min z
# column 20 = max z
# column 21 = xy-plane circumference
# column 22 = xz-plane circumference
# column 23 = yz-plane circumference
# column 24 = ratio of xz/xy-plane circumferences
# column 25 = ratio of yz/xy-plane circumferences
# column 26 = area
# column 27 = m_irreducible
# column 28 = areal radius
# column 29 = expansion Theta_(l)
# column 30 = inner expansion Theta_(n)
# column 31 = product of the expansions
# column 32 = mean curvature
# column 33 = gradient of the areal radius
# column 34 = gradient of the expansion Theta_(l)
# column 35 = gradient of the inner expansion Theta_(n)
# column 36 = gradient of the product of the expansions
# column 37 = gradient of the mean curvature
# column 38 = minimum  of the mean curvature
# column 39 = maximum  of the mean curvature
# column 40 = integral of the mean curvature
0	0.000	0.000000	-0.000000	0.000000	1.797436292	1.895002775	1.862689844	1.196781494	-4.924842382e-06	2.316410639e-07	1.178602209	0.04303624532	1.095066528	-1.894581224	1.894581224	-1.878779073	1.878779073	-1.810946378	1.810946378	12.39444633	11.73317444	11.60096862	0.9466477265	0.9359811894	45.12051695	0.9474408496	1.894881699	-2.911439429e-15	-1.469351328	4.295713763e-15	0.7346756638	0.000000000	0.000000000	0.000000000	0.000000000	0.000000000	0.7086610179	0.7502907385	33.17672077
1	0.031	0.000000	-0.000000	-0.000000	1.797372076	1.894933918	1.862625884	1.196702120	-5.225447341e-06	-1.841330198e-08	1.178522831	0.04303781043	1.094987078	-1.894534554	1.894534554	-1.878730297	1.878730297	-1.810889829	1.810889829	12.39460514	11.73324715	11.60103350	0.9466414635	0.9359744315	45.12027373	0.9474382960	1.894876592	-1.374862059e-15	-1.469190990	1.998315860e-15	0.7345954952	0.000000000	0.000000000	0.000000000	0.000000000	0.000000000	0.7086092896	0.7501493817	33.17301480
2	0.062	0.000000	0.000000	-0.000000	1.797231916	1.894796191	1.862490224	1.196534368	-5.332062717e-06	-5.596788522e-07	1.178354756	0.04303981522	1.094817729	-1.894417913	1.894417913	-1.878610152	1.878610152	-1.810757420	1.810757420	12.39476321	11.73332601	11.60110629	0.9466357539	0.9359683683	45.12014196	0.9474369125	1.894873825	-7.816026476e-14	-1.468873535	1.100771867e-13	0.7344367677	0.000000000	0.000000000	0.000000000	0.000000000	0.000000000	0.7084955844	0.7499219351	33.16583043
